#ifndef SERVER_HANDLERS_H
#define SERVER_HANDLERS_H

#include <Arduino.h>
#include <ESP8266WebServer.h>
#include <ArduinoJson.h>

#define HTTP_CODE_OK 200
#define HTTP_CODE_NOT_FOUND 404

#define JSON_CONTENT "application/json"

#define SENSORS_NUMBER 3
#define SENSOR_FIELDS 3

extern ESP8266WebServer server;
extern float Temperature;
extern float Humidity;

void handleRoot();
void handleNotFound();
void handleClient();

#endif