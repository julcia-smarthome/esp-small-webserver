#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

#include <Adafruit_Sensor.h>
#include <DHT.h>

#include "ServerHandlers.h"

#define MAC_LENGTH 6

#define ESP_LED_BUILTIN 2
#define BLINK_DELAY 100
#define READ_TEMP_DELAY 1000

#define DHTTYPE DHT11
#define DHTPin D2

#define CONNECTED_CODE 10

#define SERVER_MAIN_PATH "/"
#define SERVER_CLIENT_PATH "/read"

const String SSID = "Internet";
const String PASSWORD = "marcelina";
const int32_t CHANNEL = 11;
const uint8_t BSSIDTable[6] = {0x44, 0x78, 0x3e, 0x69, 0xfd, 0x40};
const uint8_t *BSSID = BSSIDTable;

#define SERIAL_BAUDRATE 115200

#define SERVER_PORT 80

void printFullConnectionDataOnSerial();
void blink(int times);

ESP8266WebServer server(SERVER_PORT);
DHT dht(DHTPin, DHTTYPE);                

unsigned long readTempTime;
float Temperature;
float Humidity;

void setup() 
{
    pinMode(ESP_LED_BUILTIN, OUTPUT);
    digitalWrite(ESP_LED_BUILTIN, HIGH);

    Serial.begin(SERIAL_BAUDRATE);

    WiFi.begin(SSID, PASSWORD, CHANNEL, BSSID);

    Serial.printf("\nConnecting to %s", WiFi.SSID().c_str());

    while(WiFi.status() != WL_CONNECTED){
        Serial.printf(".");
        delay(500);
    }

    server.on(SERVER_MAIN_PATH, handleRoot);
    server.on(SERVER_CLIENT_PATH, handleClient);
    server.onNotFound(handleNotFound);

    server.begin();
    blink(CONNECTED_CODE);

    printFullConnectionDataOnSerial();

    readTempTime = millis();
}

void loop() 
{
    server.handleClient();

    if ((unsigned long) millis() - readTempTime >= READ_TEMP_DELAY) {
        Temperature = dht.readTemperature();
        Humidity = dht.readHumidity();
        readTempTime = millis();
    }
}



void blink(int times)
{
    for(uint8_t i = 0; i < times; i++)
    {
        digitalWrite(ESP_LED_BUILTIN, LOW);
        delay(BLINK_DELAY);
        digitalWrite(ESP_LED_BUILTIN, HIGH);
        delay(BLINK_DELAY);
    }
}

void printFullConnectionDataOnSerial()
{
    Serial.printf("\n\nCONNECTED TO\nSSID: %s", WiFi.SSID().c_str());

    Serial.printf("\nBSSID: %x", (unsigned char)(WiFi.BSSID()[0]));
    for(uint8_t i = 1; i < MAC_LENGTH; i++)
    {
        Serial.print(":");
        Serial.printf("%x", (unsigned char)(WiFi.BSSID()[i]));
    }

    Serial.printf("\nChannel: %d", WiFi.channel());
    Serial.printf("\nDevice IP: %s", WiFi.localIP().toString().c_str());
    Serial.printf("\nSubnet mask: %s\n\n", WiFi.subnetMask().toString().c_str());
}